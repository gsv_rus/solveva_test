module.exports = {
  content: [
      './public/**/*.html',
      './src/**/*.css',
  ],
  theme: {
    extend: {
      colors: {
        'gray': '#F8F8F8',
        'light-gray': '#d6d6d6',
      },
      spacing: {
        '21px': '21px',
        '30px': '30px',
        '32px': '32px',
        '41px': '41px',
      },
      fontSize: {
        sm: ['14px','20px']
      }
    },
  },
  plugins: [
  ],
}
